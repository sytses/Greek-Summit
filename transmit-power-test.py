from microbit import *
import random
import radio

def idle_loop():
    rpower = 0
    while True:
        display.scroll(str(rpower))
        sleep(100)
        if button_a.was_pressed():
            rpower += 1
            if rpower>7:
                rpower=0
            radio.config(power=rpower)
        if button_b.was_pressed():
            radio.send('infect')
            display.scroll("Sent")
            sleep(1000)

rpower = 0
display.show(Image.YES)
radio.on()
radio.config(power=rpower)
idle_loop()
