# GitLab badge repository

Software for the micro:bit based Greek Summit badges.

## TL;DR

Your badge should have come with the standard BBC micro:bit demonstration code loaded.
If not, you can load the code onto your badge by plugging it in to a USB port and
dragging+dropping the BBC-Microbit-First-Experience file from the runtime directory
on to the Micro:bit file share.

If you would like to load the custom GitLab software onto your badge simply drag+drop
the version you would like to load.

The Micro:bit will reboot with the new code automatically.

## Prequisites

Software can be coded and the byte code downloaded here: http://microbit.org/code/

You can code locally using: https://codewith.mu/#download

Important notes:

* When uploading software via drag and drop onto the filesystem, all previous files
are erased, including any saved files on the filesystem. To avoid this always use the
microfs command "ufs" for updating your software.
* Micro:bits have a tiny amount of RAM. The Summit software bumps right up against
this memory limit. If you load software onto a badge that exceeds the memory limit
you will either recieve a memory error or the badge will refuse to boot.

## Basics

We use the MicroFS Python "pip" library for loading software onto the badges. First
a MicroPython runtime is loaded by "compiling" an empty file using [mu](https://codewith.mu/#download)
and uploading it to the badge.

Our code is then loaded using the microfs command "ufs".

```
$ ufs ls
main.py settings.txt
$ ufs get settings.txt
$ ufs put main.py
```

## Loading the summit software on to a badge

1) Connect a badge via the USB cable.

2) Load the MicroPython runtime by dragging and dropping the binary in `/runtime`.

3) Prepare the system that will be used to load the badge software onto each device:

```
sudo pip install microfs
```

If `pip` is not installed run:

```
sudo easy_install pip
```

4) Edit the `settings.txt` file and insert the badge number. The badge number should
be on the first line by itself. No other content should be in this file.

```
vi settings.txt
```

5) Load the settings onto the device and install the software:

```
ufs put settings.txt
ufs put main.py
```

If you receive an `OSError: No more storage space` error you must reinstall the
MicroPython runtime (drag+drop).

6) Eject the badge and disconnect.

## Serial port access

When a badge has only the MicroPython runtime installed (no main.py or drag/drop
software installed) you can connect to a serial port on Linux or Mac using a command such as:

```
screen /dev/cu.usbmodem1422 115200
```

Linux: `screen /dev/ttyACM0 115200`

## Using the badges

The software is controlled by the two buttons on the front of the badge.
The left button is "next" and the right button "enter".

After the badge software loads, you can enter the badge menu by hitting the
left button. Cycle through each menu option by hitting the left button. Use the right
button to make a choice.

Choose a custom emoji from the main screen by hitting the right button. Cycle
through all possible emojis using the left button and make a choice with the
right button.

To put the badge to sleep simply turn it on its head.

## Development notes

The Micro:bit has a very limited call stack. The limit on function call
depth is around 8. This means functions can be larger than you might like. It's
very un-Ruby, enjoy it.
