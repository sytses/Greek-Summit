from microbit import *
import random
import radio

version = '0.9'
badgenum = "0"
radio_on = True
standard_emojis = [ Image.HAPPY, Image.SAD, Image.ANGRY, Image.COW, Image.CONFUSED, Image.XMAS, Image.SURPRISED, Image.GHOST, Image.PACMAN ]
cur_emoji = 0
ped_count = 0
eightG_count = 0
freefall_count = 0
max_temp = 0
min_temp = 100
beacon_count = 0
gitlab_emoji = Image("90009:"
                     "99099:"
                     "99999:"
                     "09990:"
                     "00900")

def fast_scroll(msg):
    display.scroll(msg, delay=100, loop=False)

def wait_for_life(wake_on_movement):
    moved = False
    while not button_a.was_pressed() and not button_b.was_pressed() and not moved:
        if wake_on_movement and accelerometer.get_y() > -1000:
            moved = True
        sleep(1000)

def go_to_sleep():
    display.show(Image.ASLEEP)
    sleep(1000)
    radio.off()
    display.off()

def wake_up():
    radio.on()
    display.on()
    display.show(Image.HAPPY)
    sleep(1000)
    display.clear()

def check_for_sleep():
    sleep_counter = 0
    while accelerometer.get_y() < -1000:
        if sleep_counter == 2:
            display.show(Image.ASLEEP)
        sleep_counter += 1
        sleep(1000)
        if sleep_counter > 4:
          go_to_sleep()
          wait_for_life(wake_on_movement=True)
          wake_up()

def menu():
    global version, radio_on, badgenum
    menu_options = ["Sleep", "Radio Toggle", "Version", "Badge Number", "Exit"] # Exit must always be last
    menu_num = 0
    while True:
        fast_scroll(menu_options[menu_num])
        if button_b.was_pressed(): # Selection made
            if menu_num < len(menu_options)-1:
                if (menu_num == 0): # sleep
                    go_to_sleep()
                    wait_for_life(wake_on_movement=False)
                    wake_up()
                elif (menu_num == 1): # radio off
                    if (radio_on):
                        radio_on = False
                        radio.off()
                        fast_scroll("Radio now off")
                    else:
                        radio_on = True
                        radio.on()
                        fast_scroll("Radio now on")
                elif (menu_num == 2): # Version
                    fast_scroll(version)
                elif (menu_num == 3): # Badge Number
                    fast_scroll(badgenum)
            break
        if button_a.was_pressed(): # Go to next menu option
            menu_num += 1
            if menu_num > len(menu_options)-1:
                menu_num = 0
    display.clear()

def emoji_choice(choose):
    global cur_emoji
    i=0
    while i < 40: # display for 10 seconds
        display.show(standard_emojis[cur_emoji])
        if choose and button_a.was_pressed():
            cur_emoji += 1
            if cur_emoji > len(standard_emojis)-1:
                cur_emoji = 0
            i = 0
        if button_b.was_pressed():
            break
        sleep(250)
        i += 1
    display.clear()

def check_buttons():
      global beacon_count
      if button_a.was_pressed():
          menu()
      elif button_b.was_pressed():
          emoji_choice(choose=True)
      if badgenum != "0":
          msg1, msg2 = radio.receive().split(' ')
          if (len(msg2)>0):
              if msg2 == "beacon":
                  beacon_count += 1
              if random.randrange(6000) == 0:
                  radio.send(badgenum + " beacon")
                  F = open('settings.txt', 'w')
                  F.write(badgenum + '\n' + str(cur_emoji) + '\n' + str(ped_count) + '\n' + str(eightG_count) + '\n' + str(freefall_count) + '\n' + str(max_temp) + '\n' + str(min_temp) + '\n' + str(beacon_count) + '\n')
                  F.close()

def check_motion():
    global ped_count, eightG_count, freefall_count, max_temp, min_temp
    gestures = accelerometer.get_gestures()
    for i in range(len(gestures)):
        if gestures[i]=="freefall":
            freefall_count += 1
        elif gestures[i]=="shake":
            ped_count += 1
        elif gestures[i]=="8g":
            eightG_count += 1
    temp = temperature()
    if temp > max_temp:
        max_temp = temp
    if temp < min_temp:
        min_temp = temp

def check_for_input():
    check_motion()
    check_buttons()
    check_for_sleep()

def idle_loop():
    while True:
        check_for_input()
        #random_event()
        sleep(50)

display.show(gitlab_emoji)
sleep(5000)
display.clear()
try:
    FI = open('settings.txt')
    badgenum = FI.readline().rstrip()
    cur_emoji = int(FI.readline().rstrip())
    ped_count = int(FI.readline().rstrip())
    eightG_count = int(FI.readline().rstrip())
    freefall_count = int(FI.readline().rstrip())
    max_temp = int(FI.readline().rstrip())
    min_temp = int(FI.readline().rstrip())
    beacon_count = int(FI.readline().rstrip())
    fast_scroll(badgenum)
except:
    if badgenum == "0":
        fast_scroll("No Badge Number!")
radio.on()
radio.config(power=3)
idle_loop()
