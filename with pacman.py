from microbit import *
import random
import radio

version = '0.9'
badgenum = "0"
radio_on = True
standard_emojis = [ Image.HAPPY, Image.SAD, Image.ANGRY, Image.COW, Image.CONFUSED, Image.XMAS, Image.GHOST, Image.PACMAN, Image.DIAMOND, Image.STICKFIGURE ]
cur_emoji = 0
ped_count = 0
eightG_count = 0
freefall_count = 0
max_temp = 0
min_temp = 100
beacon_count = 0
last_infection = 0
gitlab_emoji = Image("90009:"
                     "99099:"
                     "99999:"
                     "09990:"
                     "00900")

def fast_scroll(msg):
    display.scroll(msg, delay=100, loop=False)

def wait_for_life(wake_on_movement):
    moved = False
    while not button_a.was_pressed() and not button_b.was_pressed() and not moved:
        if wake_on_movement and accelerometer.get_y() > -1000:
            moved = True
        sleep(1000)

def go_to_sleep():
    display.show(Image.ASLEEP)
    sleep(1000)
    radio.off()
    display.off()

def wake_up():
    radio.on()
    display.on()
    display.show(Image.HAPPY)
    sleep(1000)
    display.clear()

def check_for_sleep():
    sleep_counter = 0
    while accelerometer.get_y() < -1000:
        if sleep_counter == 2:
            display.show(Image.ASLEEP)
        sleep_counter += 1
        sleep(1000)
        if sleep_counter > 4:
          go_to_sleep()
          wait_for_life(wake_on_movement=True)
          wake_up()

def emoji_choice(choose):
    global cur_emoji
    i=0
    if choose:
        while i < 40: # display for 10 seconds
            display.show(standard_emojis[cur_emoji])
            if choose and button_a.was_pressed():
                cur_emoji += 1
                if cur_emoji > len(standard_emojis)-1:
                    cur_emoji = 0
                i = 0
            if button_b.was_pressed():
                break
            sleep(250)
            i += 1
    else:
        display.show(standard_emojis[cur_emoji])
        sleep(3000)
    display.clear()

def save_settings():
    F = open('settings.txt', 'w')
    F.write(badgenum + '\n' + str(cur_emoji) + '\n' + str(ped_count) + '\n' + str(eightG_count) + '\n' + str(freefall_count) + '\n' + str(max_temp) + '\n' + str(min_temp) + '\n' + str(beacon_count) + '\n')
    F.close()

def infect(patient_zero):
    global last_infection
    cur_time = running_time()
    if (cur_time - last_infection > 900000): # no infections within 15 mins of each other
        last_infection = cur_time
        for i in range(60):  # infectious for roughly 5 minutes (sleep 5 seconds each loop)
            display.show("Image.SKULL")
            radio.send(patient_zero + " infect")
            sleep(2500)
            if patient_zero == badgenum:
                display.scroll("Patient Zero")
            else:
                display.scroll("Infected")
            sleep(2500)

def pacman(pacman_zero):
    if badgenum == pacman_zero:
        display.show(Image.PACMAN)
        sleep(10000)
        radio.send(badgenum + "pacinit")
        for i in range(60):
            msg = radio.receive()
            if (msg == "ghost"):
                display.show(Image.GHOST)
                sleep(5000)
                break
            sleep(1000)
    else:
        display.show(Image.GHOST)
        for i in range(60):
            msg = radio.send("ghost")
            sleep(1000)
    display.clear()

def check_radio():
    global beacon_count
    if badgenum != "0":
        msg = radio.receive()
        if (msg):
            msg1,msg2,msg3 = msg.split()
            if (len(msg2)>0):
                if msg2 == "beacon":  # someone knocks
                    beacon_count += 1
                    display.show(Image.TARGET)
                    sleep(500)
                if msg2 == "infect":  # we've been infected
                    infected(msg1)
                if msg2 == "hunt":  # scavenger hunt directions
                    if (len(msg3)>0):
                        for i in range(5):
                            display.scroll(msg3)
                if msg1 == "pacinit":
                    pacman("foo")
        if random.randrange(6000) == 0:  # beacon every 5 minutes
            radio.send(badgenum + " beacon")
            save_settings()
            emoji_choice(choose=False)
            if random.randrange(144) == 0: # zombie twice a day
                infect(badgenum)
            if random.randrange(144) == 0: # pacman twice a day
                pacman(badgenum)

def check_buttons():
      if button_a.was_pressed():
          sleep(100)
      if button_b.was_pressed():
          emoji_choice(choose=True)

def check_motion():
    global ped_count, eightG_count, freefall_count, max_temp, min_temp
    gestures = accelerometer.get_gestures()
    for i in range(len(gestures)):
        if gestures[i]=="freefall":
            freefall_count += 1
        elif gestures[i]=="shake":
            display.show(Image.SUPRISED)
            sleep(100)
            display.clear()
            ped_count += 1
        elif gestures[i]=="8g":
            display.show(Image.RABBIT)
            sleep(100)
            display.clear()
            eightG_count += 1
    temp = temperature()
    if temp > max_temp:
        max_temp = temp
    if temp < min_temp:
        min_temp = temp

def check_for_input():
    check_motion()
    check_buttons()
    check_for_sleep()
    check_radio()

def idle_loop():
    while True:
        check_for_input()
        #random_event()
        sleep(50)

display.show(gitlab_emoji)
sleep(5000)
display.clear()
try:
    FI = open('settings.txt')
    badgenum = FI.readline().rstrip()
#    cur_emoji = int(FI.readline().rstrip())
#    ped_count = int(FI.readline().rstrip())
#    eightG_count = int(FI.readline().rstrip())
#    freefall_count = int(FI.readline().rstrip())
#    max_temp = int(FI.readline().rstrip())
#    min_temp = int(FI.readline().rstrip())
#    beacon_count = int(FI.readline().rstrip())
    fast_scroll(badgenum)
except:
    if badgenum == "0":
        fast_scroll("No Badge Number!")
radio.on()
radio.config(power=3)
idle_loop()
